<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
    return view('welcome');
});
Route::group(['prefix'=>'client'],function(){


    Route::get('index',function(){
        return view('client.index');
    });
    Route::get('about',function(){
        return view('client.about');
    });
    Route::get('blog',function(){
        return view('client.blog');
    });
    Route::get('account',function(){
        return view('client.my-account');
    });
    Route::get('event',function(){
        return view('client.event-detail');
    });
});


Route::group(['prefix'=>'admin'],function(){

//slide controller
Route::resource('photos','PhotosController');

//company-details controller

Route::resource('company-details','CompanysController');

//find-job controller
Route::resource('find-job','FindJobController');

//trust-us controller
Route::resource('trust-us','TrustUsController');

//event controller
Route::resource('event','EventController');

//testimonial controller
Route::resource('testimonial','TestimonialController');

//team controller
Route::resource('team','TeamController');

//dashboard controller
Route::resource('dashboard','DashboardController');

//profile controller
Route::resource('profile','ProfileController');
});





Auth::routes();
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');
