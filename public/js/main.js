$(function () {
		// Amount of scrolling before button is shown/hidden.
		var offset = 160;

		// Fade duration
		var duration = 500;

		// Toggle view of button when scrolling.
		$(window).scroll(function () {
			if ($(this).scrollTop() > offset) {
				$('#go-top').fadeIn(duration);
			} else {
				$('#go-top').fadeOut(duration);
			}
		});
	
		
	// Menu-overlay
	

		 // Login-Sign Up
		$('#login-signup').click(function () {
			$(".login-wrapper, .overlay").show();
			$('body').addClass('overflow');
			
		});
	$(".signup-btn").click(function () {
		$(".login-wrapper .left-section h2").text("Signup");
		$(".login_signup.right-section .form-container").fadeOut();
		$(".form-container.signup-form").fadeIn();
		
	});
	$(".login-btn").click(function () {
		$(".login-wrapper .left-section h2").text("Login");
		$(".login_signup.right-section .form-container").fadeIn();
		$(".form-container.signup-form").fadeOut();
		
	});
	$('.overlay, .login-wrapper .close-btn').click(function () {
		$(".login-wrapper, .overlay").hide();
		$('body').removeClass('overflow');
	});
		// Scroll to top when button is clicked.
		$('#go-top').click(function (event) {
			event.preventDefault();
			$('html, body').animate({
				scrollTop: 0
			}, duration);
			return false;
		});
	// Skill bar
	
	jQuery('.skillbar').each(function(){
	jQuery(this).find('.skillbar-bar').animate({
		width:jQuery(this).attr('data-percent')
	},2000);
});
	/*var maxHeight = -1;

            $('.login-wrapper .right-section').each(function() {
                maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
            });

            $('.login-wrapper .left-section').each(function() {
                $(this).height(maxHeight);

            });
        });	*/
	
	var maxHeight = -1;

            $('.schedule .tab-pane').each(function() {
                maxHeight = maxHeight > $(this).innerHeight() ? maxHeight : $(this).innerHeight();
            });

            $('.schedule .nav-tabs').each(function() {
                $(this).innerHeight(maxHeight);

            });
	
	 $('.blog-detail-wrapper .col-md-8').each(function() {
                maxHeight = maxHeight > $(this).innerHeight() ? maxHeight : $(this).innerHeight();
            });

            $('.blog-detail-wrapper .vertical-center').each(function() {
                $(this).innerHeight(maxHeight);

            });
        
 });


// Focus Input Label
$(document).ready(function() {
    var $input = $('.form-control');

    $input.focusout(function() {
        if($(this).val().length > 0) {
            $(this).next('label').addClass('focus-label');
        }
        else {
            $(this).next('label').removeClass('focus-label');
        }
    });
	
	$('input').on('blur', function(){
   		$(this).parent().removeClass('focused');
	}).on('focus', function(){
	   $(this).parent().addClass('focused');
	});
	

});

$(document).ready(function(){
	
	
	// Dropdown
	"use strict";
    var aChildren = $(".event-tabs li,.search-result-wrapper .about-tabs li").children(); // find the a children of the list items
    var aArray = []; // create the empty aArray
    for (var i=0; i < aChildren.length; i++) {    
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values

    $(window).scroll(function(){
        var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
        var windowHeight = $(window).innerHeight(); // get the height of the window
        var docHeight = $(document).innerHeight();

        for (var i=0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top-100; // get the offset of the div from the top of page
            var divHeight = $(theID).innerHeight(); // get the height of the div in question
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("active");
            } else {
                $("a[href='" + theID + "']").removeClass("active");
            }
        }

        if(windowPos + windowHeight === docHeight) {
            if (!$(".event-tabs li:last-child a, .search-result-wrapper .about-tabs li a").hasClass("active")) {
                var navActiveCurrent = $(".active").attr("href");
                $("a[href='" + navActiveCurrent + "']").removeClass("active");
                $(".event-tabs li:last-child a, .search-result-wrapper .about-tabs li a").addClass("active");
            }
        }
    });
});
// Disable Swiper at 768px

		
/* Event Page Scroller Active Class */
!function(a){"use strict";a('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function(){if(location.pathname.replace(/^\//,"")==this.pathname.replace(/^\//,"")&&location.hostname==this.hostname){var e=a(this.hash);if((e=e.length?e:a("[name="+this.hash.slice(1)+"]")).length)return a("html, body").animate({scrollTop:e.offset().top-95},1e3,"easeInOutExpo"),!1}}),a(".js-scroll-trigger").click(function(){a(".navbar-collapse").collapse("hide")}),a("body").scrollspy({target:"#mainNav",offset:54});var e=function(){a("#mainNav").offset().top>100?a("#mainNav").addClass("navbar-shrink"):a("#mainNav").removeClass("navbar-shrink")};e(),a(window).scroll(e)}(jQuery);
