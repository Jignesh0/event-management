// Hero Slider 	
var swiper = new Swiper('.swiper-container-home', {
    loop: true,
    speed: 700,
    pagination: {
  el: '.swiper-pagination',
        clickable: true,
},
navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
  },
});	

// Event Slider	
var x = document.getElementsByClassName("swiper-event");

for(var i = 0; i < x.length; i++) {

var el = x[i];

var swiper = el.getElementsByClassName("swiper-container")[0];
var nx = el.getElementsByClassName("swiper-next")[0];
var pr = el.getElementsByClassName("swiper-prev")[0];

new Swiper(swiper, {
  slidesPerView: 4,  
  spaceBetween: 14,
  navigation: {
    nextEl: nx,
    prevEl: pr
  },
breakpoints: {
  1024: {
      slidesPerView: 3,
      spaceBetween: 10,
  },
  800: {
      slidesPerView: 2,
      spaceBetween: 10,
  },
  768: {
      slidesPerView: 2.4,
      
  },
    414: {
    slidesPerView: 1.4,
}
}
});
}
// Companies Slider

var y = document.getElementsByClassName("swiper-company");

for(var i = 0; i < y.length; i++) {

var el = y[i];

var swiper = el.getElementsByClassName("swiper-container")[0];
var nx = el.getElementsByClassName("swiper-next")[0];
var pr = el.getElementsByClassName("swiper-prev")[0];

new Swiper(swiper, {
  slidesPerView: 5,  
  spaceBetween: 14,
  navigation: {
    nextEl: nx,
    prevEl: pr
  },
pagination: {
  el: '.swiper-pagination',
        clickable: true,
},
breakpoints: {
    768: {
        slidesPerView: 2.4,
    },
    500: {
        slidesPerView: 2.1,
    }
}
});
}
// Upcoming Event Slider
var swiper = new Swiper('.swiper-container-upcoming-events', {
  slidesPerView: 1,
pagination: {
  el: '.swiper-pagination',
        clickable: true,
},

});	

// Testimonials
var swipertestimonial = new Swiper('.swiper-container-testimonial', {
slidesPerView: 3,
spaceBetween: 38,
pagination: {
  el: '.swiper-pagination',
        clickable: true,
},
breakpoints: {
    768: {
        slidesPerView: 2,
        spaceBetween: 0,
    },
  500: {
        slidesPerView: 1.5,
        spaceBetween: 0,
    }
}
});
// Update slider instance when using tabs	
$('.testimonial .nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
var paneTarget = $(e.target).attr('href');
var $thePane = $('.tab-pane' + paneTarget);
var paneIndex = $thePane.index();
if ($thePane.find('.swiper-container').length > 0 && 0 === $thePane.find('.swiper-slide-active').length) {
swipertestimonial[paneIndex].update();
}
});
// Our Team
var swiper = new Swiper('.swiper-container-team', {
slidesPerView: 3,
spaceBetween: 38,
pagination: {
  el: '.swiper-pagination',
        clickable: true,
},
breakpoints: {
    768: {
        slidesPerView: 1.8,
        spaceBetween: 10,
        
    },
  500: {
      slidesPerView: 1.5,
      spaceBetween: 13,
  }
}

});

$(".collapsed").click(function() {
        setTimeout(function() {
          $(".menu-overlay").toggleClass("show");
        }, 400);
      });
     
      $(".menu-overlay").click(function() {
              $(this).removeClass("show");
          $('body').removeClass('overflow');
          $(".navbar-collapse").removeClass('in');
      });


$(window).on('load', function(){
  $('#overlay').fadeOut();
});