<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable = array( 'name','position','type','discription','image',);


    public function photos(){
      return $this->hasMany('App\Photo');
}
}