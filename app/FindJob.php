<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FindJob extends Model
{
   protected $fillable = array( 'title','image','discription');


    public function photos(){
      return $this->hasMany('App\Photo');
  }
}
