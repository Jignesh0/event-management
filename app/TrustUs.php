<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrustUs extends Model
{
	protected $table = 'company_trusts';
    protected $fillable = array( 'name','company_logo');
    public function photos(){
      return $this->hasMany('App\Photo');
}
}