<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
     protected $fillable = array( 'name','discription','image','logo');


    public function photos(){
      return $this->hasMany('App\Photo');
}
}
