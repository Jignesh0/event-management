<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = array('slide_id', 'text', 'photo', 'size');

    public function slide(){
      return $this->belongsTo('App\Slide');
    }
    public function comapny(){
      return $this->belongsTo('App\CompanyDetails');
    }

    public function findjob(){
      return $this->belongsTo('App\FindJob');
    }

    public function testimonial(){
      return $this->belongsTo('App\Testimonial');
    }

     public function team(){
      return $this->belongsTo('App\Team');
    }

    public function company_trusts(){
      return $this->belongsTo('App\TrustUs');
    }

    public function event(){
      return $this->belongsTo('App\Event');
    }
     public function admin(){
      return $this->belongsTo('App\Admin');
    }
}
