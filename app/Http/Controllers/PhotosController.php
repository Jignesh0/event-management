<?php

namespace App\Http\Controllers;
use App\User;
use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PhotosController extends Controller
{
    public function index(){
    	$users = User::all();  
    	$slides = Slide::orderBy('created_at','desc')->get();
      return view('admin.slide.index',compact('usrs','slides'));
    }

    public function create(){
    	
    $users = User::all();
    $slides = Slide::all();
      return view('admin.slide.create',compact('users','slides'));
    }

    
      public function store(Request $request){
        $this->validate($request, [
        'name' => 'required',
        'cover_image' => 'image|max:1999',
        'cover_image' => 'required'
      ]);

      // Get filename with extension
      $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('cover_image')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('cover_image')->move('album_covers', $filenameToStore);

      // Create album
      $slide = new Slide;
      $slide->text = $request->input('name');
     
      $slide->cover_image = $filenameToStore;

      $slide->save();
      Session::flash('message', 'My message');
      return redirect('admin/photos')->with('success', 'slide Added');
    }

    public function destroy($id){
      $slide = Slide::find($id);
       File::delete('album_covers/' . $slide->cover_image);
       $slide->delete();
      return redirect('admin/photos')->with('warning', 'slide Deleted');
    }

     public function edit($id)
    {
    	$users = User::all();	
        $slides = Slide::find($id);
        return view('admin.slide.edit',compact('slides','users'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $slide = Slide::find($id);
         $slide->update($request->all());
       
        $slide->text = $request->input('name');
     	$slide->cover_image=request('cover_image');
     
       

        $slide->save();

        return redirect('admin/photos')->with('info','slide Upated');
    }
    
}

