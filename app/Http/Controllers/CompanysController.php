<?php

namespace App\Http\Controllers;
use App\CompanyDetails;
use App\User;
use App\Slide;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CompanysController extends Controller
{
    public function index(){
    	 $users = User::all();
    	 $companydetails = CompanyDetails::orderBy('created_at','desc')->get();
      return view('admin.company-details.index',compact('users','companydetails'));
    }

    public function create(){
    	
      $users = User::all();
    $companydetails = CompanyDetails::all();
   
      return view('admin.company-details.create',compact('users','companydetails'));
}

 public function store(Request $request){
        $this->validate($request, [
        'company_name' => 'required',
        'company_photo' => 'image|max:1999',
        'company_photo' => 'required',
        'start_date'=>'required',
        'end_date'=>'required'
      ]);

      // Get filename with extension
      $filenameWithExt = $request->file('company_photo')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('company_photo')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('company_photo')->move('company', $filenameToStore);

      // Create album
      $companydetail = new CompanyDetails;

      $companydetail->company_name = $request->input('company_name');
      $companydetail->company_photo = $filenameToStore;
     $companydetail->start_date = $request->input('start_date');
     $companydetail->end_date = $request->input('end_date');
     
      

      $companydetail->save();

      return redirect('admin/company-details')->with('success', 'Company details Added');
    }

    public function destroy($id){
      $companydetail = CompanyDetails::find($id);
      File::delete('company/' . $companydetail->company_photo);
      	$companydetail->delete();
    
      
      return redirect('admin/company-details')->with('warning', 'Company details Deleted');
    }

     public function edit($id)
    {
    	$users = User::all();	
         $companydetails = CompanyDetails::find($id);
        return view('admin.company-details.edit',compact('companydetails','users'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $companydetail = CompanyDetails::find($id);
        $companydetail->update($request->all());
        $companydetail->company_name=request('company_name');
        $companydetail->company_photo=request('company_photo');
        $companydetail->start_date = $request->input('start_date');
     $companydetail->end_date = $request->input('end_date');

        $companydetail->save();

        return redirect('admin/company-details')->with('info','Company details Upated');
    }
}
