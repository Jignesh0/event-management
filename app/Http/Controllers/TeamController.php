<?php

namespace App\Http\Controllers;
use App\User;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TeamController extends Controller
{
    public function index(){
    	 $users = User::all();
    	 $teams = Team::orderBy('created_at','desc')->get();
      return view('admin.team.index',compact('users','teams'));
    }

    public function create(){
    	
    $users = User::all();
    $teams = Team::all();
      return view('admin.team.create',compact('users','teams'));
    }

    public function store(Request $request){

     $this->validate($request, [
        'name' => 'required',
        'designation' => 'required',
        'image' => 'image|max:1999',
        'image' => 'required',
        'description' => 'required'
      ]);

      // Get filename with extension
      $filenameWithExt = $request->file('image')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('image')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('image')->move('team_img', $filenameToStore);

      // Create album
      $team = new Team;
      $team->name = $request->input('name');
      $team->designation = $request->input('designation');
      $team->description = $request->input('description');
      $team->image = $filenameToStore;
      

      $team->save();

      return redirect('admin/team')->with('success', 'Team Added');
    }

    public function destroy($id){
      $team = Team::find($id);
       File::delete('team_img/' . $team->image);
       $team->delete();
      return redirect('admin/team')->with('warning', 'Team Deleted');
    }

     public function edit($id)
    {
    	$users = User::all();	
        $teams = Team::find($id);
        return view('admin.team.edit',compact('teams','users'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $team = Team::find($id);
         $team->update($request->all());
         $team->name = $request->input('name');
      $team->designation = $request->input('designation');
      $team->description = $request->input('description');
     
      $team->image=request('image');
       

        $team->save();

        return redirect('admin/team')->with('info','Team Upated');
    }
}
