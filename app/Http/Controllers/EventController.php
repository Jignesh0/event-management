<?php

namespace App\Http\Controllers;
use App\User;
use App\Event;
use App\CompanyDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class EventController extends Controller
{
    public function index(){
    	$users = User::all();
      $companydetails = CompanyDetails::pluck('company_name','id');
    $events = Event::orderBy('created_at','desc')->get();
      return view('admin.event.index',compact('users','events','companydetails'));
    }

    public function create(){
    	
    $users = User::all();
    
    $events = Event::all();
      return view('admin.event.create',compact('users','events'));
    }

    public function store(Request $request){

     $this->validate($request, [
        'events_title' => 'required',
        
        'image' => 'image|max:1999',
         'image' => 'required',
        'description' => 'required'
      ]);

      /*		
     Get filename with extension
      $filenameWithExt = $request->file('logo')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('logo')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('logo')->move('event_img/logo', $filenameToStore);
*/

       //*/................Image............./*//
        // Get filename with extension
     $filenameWithExt = $request->file('image')->getClientOriginalName();

      // Get just the filename
   	 $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('image')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('image')->move('event_img/image', $filenameToStore);

      // Create album
      $event = new Event;
      $event->events_title = $request->input('events_title');
        $event->company_id = $request->input('company_id');
      $event->description = $request->input('description');
     
      $event->image = $filenameToStore;
      

      $event->save();

      return redirect('admin/event/create')->with('success', 'Event Added');
    }

    public function destroy($id){
      $event = Event::find($id);
      File::delete('event_img/image/' . $event->image);
      File::delete('event_img/logo/' . $event->logo);
       $event->delete();
      return redirect('admin/event')->with('warning', 'Event Deleted');
    }

    public function edit($id)
    {
    	$users = User::all();	
        $events = Event::find($id);
         $companydetails = CompanyDetails::pluck('company_name','id');
        return view('admin.event.edit',compact('events','users','companydetails'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $event = Event::find($id);
        $event->update($request->all());
        $event->events_title = $request->input('events_title');
         $event->description = $request->input('description');
        $event->image=request('image');
        $event->company_id=request('company_id');
      
       

        $event->save();

        return redirect('admin/event')->with('info','Event Upated');
    }


    
}
