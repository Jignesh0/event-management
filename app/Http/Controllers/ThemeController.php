<?php

namespace App\Http\Controllers;
use App\Slide;
use App\User;
use App\CompanyDetails;
use App\FindJob;
use App\TrustUs;
use App\Testimonial;
use App\Team;
use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ThemeController extends Controller
{
    public function index(){

      $slides = Slide::with('Photos')->get();
      $companydetails = CompanyDetails::all();
      $findjobs = FindJob::all();
      $trustuss = TrustUs::all();

      $testimonials = DB::table('testimonials')->where('type', 'Developers')->get();
      $testis = DB::table('testimonials')->where('type', 'Business')->get();

      $teams = Team::all();
      $events = Event::all();
      return view('theme.index',compact('slides','findjobs','companydetails','trustuss','testimonials','testis','teams','events'));
    }
}
