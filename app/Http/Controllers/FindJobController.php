<?php

namespace App\Http\Controllers;

use App\User;
use App\FindJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FindJobController extends Controller
{
     public function index(){
     	$users = User::all();
     	$findjobs = FindJob::orderBy('created_at','desc')->get();
      return view('admin.find-job.index',compact('users','findjobs'));
    }

    public function create(){
    	
    $users = User::all();
   
    $findjobs = FindJob::all();
      return view('admin.find-job.create',compact('users','findjobs'));
    }

    public function store(Request $request){
     $this->validate($request, [
        'title' => 'required',
        'image' => 'required',
        'description' => 'required'
      ]);

      // Get filename with extension
      $filenameWithExt = $request->file('image')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('image')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('image')->move('find_job', $filenameToStore);

      // Create album
      $findjob = new FindJob;
      $findjob->title = $request->input('title');
     
      $findjob->image = $filenameToStore;
      $findjob->description = $request->input('description');

      $findjob->save();

      return redirect('admin/find-job')->with('success', 'Find Job Added');
    }

    public function destroy($id){
      $findjob = FindJob::find($id);
       File::delete('find_job/' . $findjob->image);
       $findjob->delete();
      return redirect('admin/find-job')->with('warning', 'Find Job Deleted');
    }

     public function edit($id)
    {
    	$users = User::all();	
        $findjobs = FindJob::find($id);
        return view('admin.find-job.edit',compact('findjobs','users'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $findjob = FindJob::find($id);
        $findjob->update($request->all());
        $findjob->title=request('title');
        $findjob->image=request('image');
        $findjob->description=request('description');
       

        $findjob->save();

        return redirect('admin/find-job')->with('info','Find Job Upated');
    }
}
