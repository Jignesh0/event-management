<?php

namespace App\Http\Controllers;
use App\User;

use App\TrustUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class TrustUsController extends Controller
{
     public function index(){
     	$users = User::all();
     	$company_trusts = TrustUs::orderBy('created_at','desc')->get();
      return view('admin.trust-us.index',compact('usrs','company_trusts'));
    }

    public function create(){
    	
    $users = User::all();
    
  
    $trustuss = TrustUs::all();
      return view('admin.trust-us.create',compact('users','trustuss'));
    }

    public function store(Request $request){
     $this->validate($request, [
       
        'company_logo' => 'image|max:1999',
        'company_logo' => 'required',
        
      ]);

      // Get filename with extension
      $filenameWithExt = $request->file('company_logo')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('company_logo')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('company_logo')->move('company_logo', $filenameToStore);

      // Create album
      $trustus = new TrustUs;
     
     
      $trustus->company_logo = $filenameToStore;
     

      $trustus->save();

      return redirect('admin/trust-us')->with('success', 'Trust-us partners Added');
    }

    public function destroy($id){
      $trustus = TrustUs::find($id);
      File::delete('company_logo/' . $trustus->company_logo);
       $trustus->delete();
      return redirect('admin/trust-us')->with('warning', 'Trust-us partners Deleted');
    }

     public function edit($id)
    {
    	$users = User::all();	
        $trustuss = TrustUs::find($id);
        return view('admin.trust-us.edit',compact('trustuss','users'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $trustus = TrustUs::find($id);
         $trustus->update($request->all());
      
      $trustus->company_logo=request('company_logo');
     
       

        $trustus->save();

        return redirect('admin/trust-us')->with('info','Trust-us partners Upated');
    }
}
