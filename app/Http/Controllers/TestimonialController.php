<?php

namespace App\Http\Controllers;
use App\User;
use App\Testimonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TestimonialController extends Controller
{
    public function index(){
    	$users = User::all();
    	$testimonials = Testimonial::orderBy('created_at','desc')->get();
      return view('admin.testimonial.index',compact('users','testimonials'));
    }

    public function create(){
    	
    $users = User::all();
   	$testimonials = Testimonial::all();	
      return view('admin.testimonial.create',compact('users','testimonials'));
    }

    public function store(Request $request){

     $this->validate($request, [
        'name' => 'required',
        'position' => 'required',
        'type' => 'required',
        'image' => 'image|max:1999',
        'image' => 'required',
        'description' => 'required'
      ]);

      // Get filename with extension
      $filenameWithExt = $request->file('image')->getClientOriginalName();

      // Get just the filename
      $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

      // Get extension
      $extension = $request->file('image')->getClientOriginalExtension();

      // Create new filename
      $filenameToStore = $filename.'_'.time().'.'.$extension;

      // Uplaod image
       $path= $request->file('image')->move('testimonial_img', $filenameToStore);

      // Create album
      $testimonial = new Testimonial;
      $testimonial->name = $request->input('name');
      $testimonial->position = $request->input('position');
      $testimonial->type = $request->input('type');
      $testimonial->description = $request->input('description');
      $testimonial->image = $filenameToStore;
      

      $testimonial->save();

      return redirect('admin/testimonial')->with('success', 'Testimonial Added');
    }

    public function destroy($id){
      $testimonial = Testimonial::find($id);
       File::delete('testimonial_img/' . $testimonial->image);
       $testimonial->delete();
      return redirect('admin/testimonial')->with('warning', 'Testimonial Deleted');
    }

     public function edit($id)
    {
    	$users = User::all();	
        $testimonials = Testimonial::find($id);
        return view('admin.testimonial.edit',compact('testimonials','users'));
    }


     public function update(Request $request, $id)
    {
        $users = User::all();
        $testimonial = Testimonial::find($id);
         $testimonial->update($request->all());
        $testimonial->name = $request->input('name');
      $testimonial->position = $request->input('position');
      $testimonial->type = $request->input('type');
      $testimonial->description = $request->input('description');
     
      $testimonial->image=request('image');
       

        $testimonial->save();

        return redirect('admin/testimonial')->with('info','Testimonial Upated');
    }
}
