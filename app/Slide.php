<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $fillable = array('text', 'cover_image');

    public function photos(){
      return $this->hasMany('App\Photo');
  }
}
