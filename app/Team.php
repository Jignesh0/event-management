<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = array( 'name','status','discription','image',);


    public function photos(){
      return $this->hasMany('App\Photo');
}
}
