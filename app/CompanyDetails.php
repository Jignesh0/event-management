<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CompanyDetails extends Model
{
    protected $fillable = array( 'company_name','company_photo','date','time');


    public function photos(){
      return $this->hasMany('App\Photo');
}
public function getTimeAttribute($value){
        return Carbon::parse($value)->format('g:i A');
    }

public function getDateAttribute($value){
        return Carbon::parse($value)->format('d-m-Y');
    }

}
