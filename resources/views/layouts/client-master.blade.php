<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home | Incubateind</title>
<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/style.css')}}">
<link rel="stylesheet" href="{{ asset('css/swiper.min.css')}}">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
@yield('page-css')
</head>

<body>
<div class="overlay"></div>
  <div id="overlay">
    <div class="loader">
      <div class="circle"></div>
      <div class="circle"></div>
      <div class="circle"></div>
    </div>
  </div>
  <!--<div class="chat"><a href="#"><img src="img/chat.svg" alt=""></a></div>-->
  <!-- Header-->
  <header>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
		  <div class="menu-overlay"></div>
		<!-- Logo & Hamburger Menu -->
        <div class="navbar-header">
          <div class="hamburger">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <a class="navbar-brand" href="index.html"><img src="{{asset('img/logo.svg')}}" alt=""></a>
		</div>

		<!-- Login & Signup -->
		<div class="login-signup">
			<!-- Before Login -->
			<div class="login">
				@if(Auth::user())
					<a href="{{ route('logout') }}">LogOut</a>
				@else
				<a href="#" id="login-signup">Login/Sign up</a>
				@endif
			</div>
			<!-- After Login -->
			<!--<ul class="logged-in">
				<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><img src="img/default-profile.svg" alt=""><img src="img/down-arrow.svg" alt="" class="down-arrow"></a>
					<ul class="dropdown-menu">
						<h3>Hi,</h3>
						<p>Kapoor.varun4@gmail.com</p>
						<a href="index.html" class="border-button">Sign out</a>
					</ul>
				</li>
		    </ul> -->
	   </div>
		<!-- Menu -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <!-- Search bar -->
		  <div class="search-bar">
			  <form action="search-results.html">
		  		<div class="dropdown"><input type="search" placeholder="Search for an Events" data-toggle="drodown" class="dropdown-toggle">
				 <div class="blogs popular-search">
					<ul class="dropdown-menu">
						<p>Popular Search</p>
						<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/one.png')}}" alt=""></div><div class="elipsis-text"><h4>Using Artificial Intelligence, Chatbots will be imaginative – Gaurav</h4></div></a></li>
						<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/two.png')}}" alt=""></div><div class="elipsis-text"><h4>Technologies like Artificial Intelligence can make rental market Robus</h4></div></a></li>
						<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/three.png')}}" alt=""></div><div class="elipsis-text"><h4>Big Data and Artificial Intelligence are changing the world in a dram</h4></div></a></li>
						<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/four.png')}}" alt=""></div><div class="elipsis-text"><h4>Maharashtra`s IT Dept signs a MoU with University of Southern California</h4></div></a></li>
			 		 </ul>
				</div>
				 </div>
		     </form>
		  </div>
          <ul class="nav navbar-nav pull-right" id="mainNav">
            <li><a href="about-us.html">About Us</a></li>
            <li class="blogs events dropdown"><a href="event-detail.html" class="dropdown-toggle">Events <img src="{{asset('img/down-arrow.svg')}}" alt=""></a>
			  <ul class="dropdown-menu">
				<li><a href="#"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/one.png')}}" alt=""></div><h3>Intel</h3><div class="elipsis-text"><h4>Using Artificial Intelligence, Chatbots</h4></div><p>upcoming event</p> </a></li>
				<li><a href="#"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/two.png')}}" alt=""></div><h3>Microsoft</h3><div class="elipsis-text"><h4>Technologies like Artificial</h4></div><p>Upcoming Event</p></a></li>
				<li><a href="#"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/three.png')}}" alt=""></div><h3>Dish a thon</h3><div class="elipsis-text"><h4>Big Data and Artificial Intelligence</h4></div><p>Upcoming Event</p></a></li>
				<li><a href="#"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/four.png')}}" alt=""></div><h3>Vistara</h3><div class="elipsis-text"><h4>Maharashtr`s IT Dept</h4></div><p>Upcoming Event</p></a></li>
				<li class="see-all"><a href="search-results.html" class="border-button">see all events</a></li>
			  </ul>
			</li>
            <li class="blogs dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blogs <img src="{{asset('img/down-arrow.svg')}}" alt=""></a>
			  <ul class="dropdown-menu">
				<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/one.png')}}" alt=""></div><div class="elipsis-text"><h4>Using Artificial Intelligence, Chatbots will be imaginative – Gaurav</h4></div></a></li>
				<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/two.png')}}" alt=""></div><div class="elipsis-text"><h4>Technologies like Artificial Intelligence can make rental market Robus</h4></div></a></li>
				<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/three.png')}}" alt=""></div><div class="elipsis-text"><h4>Big Data and Artificial Intelligence are changing the world in a dram</h4></div></a></li>
				<li><a href="blog-detail.html"><div class="thubnail"><img src="{{asset('img/blog/thumbnail/four.png')}}" alt=""></div><div class="elipsis-text"><h4>Maharashtra`s IT Dept signs a MoU with University of Southern California</h4></div></a></li>
				  <li class="see-all"><a href="blog.html" class="border-button">see all blogs</a></li>
			  </ul>
			</li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <!-- Site Wrapper -->
<div class="site-wrapper">
@yield('content')
</div>

	<!-- Footer -->
	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="logo"><a href="index.html"><img src="{{asset('img/logo-white.svg')}}" alt="Logo"></a></div>
				  <div class="keep-in-touch">
						<h6>Keep in touch</h6>
					<ul class="social-links">
							<li><a href="https://www.linkedin.com/company/incubateind?originalSubdomain=in" target="_blank"><img src="{{asset('img/linkedin.svg')}}" alt="linkedin"></a></li>
							<li><a href="https://twitter.com/incubateind?lang=en"><img src="{{asset('img/twitter.svg')}}" alt="twitter"></a></li>
							<li><a href="https://www.youtube.com/channel/UCcgyWC65tim3f3SMSZaMQ2w"><img src="{{asset('img/youtube.svg')}}" alt="youtube"></a></li>
							<li><a href="https://www.instagram.com/incubateindia/"><img src="{{asset('img/instagram.svg')}}" alt="instagram"></a></li>
							<li><a href="#"><img src="{{asset('img/telegram.svg')}}" alt="telegram"></a></li>
					  </ul>
					</div>
				</div>
				<div class="col-md-8">
					<div class="col-md-3">
						<h6>Company</h6>
						<ul>
							<li><a href="javascript:void(0)">About Us</a></li>
							<li><a href="javascript:void(0)">Careers</a></li>
							<li><a href="javascript:void(0)">Blog</a></li>
							<li><a href="javascript:void(0)">Privacy Policy</a></li>
							<li><a href="javascript:void(0)">Contact Us</a></li>
						</ul>
					</div>
					<div class="col-md-4 col-push-1">
						<h6>Developers</h6>
						<ul>
							<li><a href="javascript:void(0)">Scoring</a></li>
							<li><a href="javascript:void(0)">Environment</a></li>
							<li><a href="javascript:void(0)">FAQ</a></li>
							<li><a href="javascript:void(0)">For Schools</a></li>
							<li><a href="javascript:void(0)">Sign up</a></li>
						</ul>
					</div>
					<div class="col-md-3 col-offset-1">
						<h6>Companies</h6>
						<ul>
							<li><a href="javascript:void(0)">Solutions</a></li>
							<li><a href="javascript:void(0)">Customers</a></li>
							<li><a href="javascript:void(0)">Pricing</a></li>
							<li><a href="javascript:void(0)">Try for Free</a></li>
							<li><a href="javascript:void(0)">Terms of Service</a></li>
						</ul>
					</div>
					<div class="col-md-2">
						<h6>Resources</h6>
						<ul>
							<li><a href="javascript:void(0)">API</a></li>
							<li><a href="javascript:void(0)">Guides</a></li>
							<li><a href="javascript:void(0)">Partners</a></li>
							<li><a href="javascript:void(0)">Events</a></li>
							<li><a href="javascript:void(0)">News</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="copyright">Copyright &copy; 2018 IncubateIND.Com, All rights reserved.</div>
		</div>
		 <div id="go-top"> <a href="#"><img src="{{asset('img/arrow-up.svg')}}" alt=""> </a></div>
	</footer>
<script src="{{asset('js/jquery-3.3.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/swiper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/main.js')}}" type="text/javascript"></script>
<script src="{{asset('js/common-slider.js')}}" type="text/javascript"></script>

</body>
</html>
