<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
    <title>Espire - Bootstrap 4 Admin Template</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/public/images/logo/favicon.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <!-- plugins css -->

    <!-- core css -->
    <link href="{{asset('css/ei-icon.css')}}" rel="stylesheet">
    <link href="{{asset('css/themify-icons.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet" >
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">


</head>

<body>
    <div class="app">
        <div class="layout">
            <!-- Side Nav START -->
            <div class="side-nav">
                <div class="side-nav-inner">
                    <div class="side-nav-logo">
                        <a href="{{route('dashboard.index')}}">
                            <div class="logo logo-dark" style="background-image: url('/images/logo/logo.png')"></div>
                            <div class="logo logo-white" style="background-image: url('/images/logo/logo-white.png')"></div>
                        </a>
                        <div class="mobile-toggle side-nav-toggle">
                            <a href="">
                                <i class="ti-arrow-circle-left"></i>
                            </a>
                        </div>
                    </div>
                    <ul class="side-nav-menu scrollable">
                        <li class="nav-item active">
                            <a class="mrg-top-30" href="{{route('dashboard.index')}}">
                                <span class="icon-holder">
										<i class="ti-home"></i>
									</span>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('photos.index')}}">
                                <span class="icon-holder">
                                        <i class="ti-plus"></i>
                                    </span>
                                <span class="title">Add Slide</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('company-details.index')}}">
                                <span class="icon-holder">
                                        <i class="ti-info"></i>
                                    </span>
                                <span class="title">Company details</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('find-job.index')}}">
                                <span class="icon-holder">
                                        <i class="fa fa-tasks"></i>
                                    </span>
                                <span class="title">Find Job</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('trust-us.index')}}">
                                <span class="icon-holder">
                                        <i class="fa fa-handshake-o"></i>
                                    </span>
                                <span class="title">Trust Us Partners</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('event.index')}}">
                                <span class="icon-holder">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                <span class="title">Events</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('testimonial.index')}}">
                                <span class="icon-holder">
                                        <i class="fa fa-quote-left"></i>
                                    </span>
                                <span class="title">Testimonials</span>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="text" href="{{route('team.index')}}">
                                <span class="icon-holder">
                                        <i class="fa fa-group"></i>
                                    </span>
                                <span class="title">Our Teams</span>
                            </a>
                        </li>

                     </ul>

				</div>
            </div>
            <!-- Side Nav END -->

            <!-- Page Container START -->
            <div class="page-container">
                <!-- Header START -->
                <div class="header navbar">
                    <div class="header-container">
                        <ul class="nav-left">
                            <li>
                                <a class="side-nav-toggle" href="javascript:void(0);">
                                    <i class="ti-view-grid"></i>
                                </a>
                            </li>


                        </ul>
                        <ul class="nav-right">
                            <li class="user-profile dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                    <img class="profile-img img-fluid" src="/images/user.jpg" alt="">

                                </a>
                                <ul class="dropdown-menu">

                                    <li>
                                        <a href="{{route('profile.index')}}">
                                            <i class="ti-user pdd-right-10"></i>
                                            <span>Profile</span>
                                        </a>
                                    </li>

                                    <li role="separator" class="divider"></li>
                                     <li>
                                        <a href="account.php">
                                            <i class="ti-power-off pdd-right-10"></i>
                                            <span>Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>




                        </ul>
                    </div>
                </div>
                <!-- Header END -->







                <!-- Content Wrapper START -->

                @yield('content')
                <!-- Content Wrapper END -->

                <!-- Footer START -->
                <footer class="content-footer">
                    <div class="footer">
                        <div class="copyright">
                            <span>Copyright © 2017 <b class="text-dark">Theme_Nate</b>. All rights reserved.</span>
                            <span class="go-right">
									<a href="" class="text-gray mrg-right-15">Term &amp; Conditions</a>
									<a href="" class="text-gray">Privacy &amp; Policy</a>
								</span>
                        </div>
                    </div>
                </footer>
                <!-- Footer END -->

            </div>
            <!-- Page Container END -->

        </div>
    </div>


    <!-- core js -->
    <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
 <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
      <script src="{{asset('js/data-table.js')}}" type="text/javascript"></script>
      <script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
      <script src="{{asset('js/perfect-scrollbar.jquery.js')}}"></script>
      <script src="{{asset('js/perfect-scrollbar.jquery.min.js')}}"></script>
      <script src="{{asset('js/perfect-scrollbar.js')}}"></script>
      <script src="{{asset('js/perfect-scrollbar.min.js')}}"></script>
        <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <script >
    $('#input_time').datepicker({
           dateFormat: 'dd-mm-yy',
            changeMonth: true,
          changeYear: true,
          yearRange: '1950:2050',
          showButtonPanel: true,
            maxDate: '0',
            onSelect: function(datetext){
                var d = new Date(); // for now
                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;
                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;
                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;
                datetext = datetext + " " + h + ":" + m + ":" + s;
                $('#input_time').val(datetext);
            },
    });
     $('#input_endtime').datepicker({
           dateFormat: 'dd-mm-yy',
            changeMonth: true,
          changeYear: true,
          yearRange: '1950:2050',
          showButtonPanel: true,
            maxDate: '0',
            onSelect: function(datetext){
                var d = new Date(); // for now
                var h = d.getHours();
                h = (h < 10) ? ("0" + h) : h ;
                var m = d.getMinutes();
                m = (m < 10) ? ("0" + m) : m ;
                var s = d.getSeconds();
                s = (s < 10) ? ("0" + s) : s ;
                datetext = datetext + " " + h + ":" + m + ":" + s;
                $('#input_endtime').val(datetext);
            },
    });
</script>

      <script>
     $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    </script>
    @section('page-js')

    <script src="js/bootstrap-datepicker.js"></script>

    <!-- page js -->
    <script >
         $( document ).ready(function() {
          $('#input_starttime').timepicker({

    // 12 or 24 hour
    twelvehour: true,
    timeFormat: 'h:mm p',
    dynamic: true,
    dropdown: true,
    scrollbar: true
});
    });

    </script>
 
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>




@stop

</body>

</html>