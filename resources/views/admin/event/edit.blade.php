@extends('layouts.admin-master')
                             <!-- Content Wrapper START -->
@section('content')
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="card">
                           <div class="col-md-12">
            <div class="panel panel-default">

              <div class="panel-body">
                
                <div class="tab-content br-n pn">
                 
                    <div class="row">
                      <div class="col-md-8"><br>
                                  {!!Form::open(['method' => 'POST','action' =>[ 'EventController@update', $events->id]])!!}
    <div class="form-group">
        <div class="col-sm-8">
         
     {!! Form::label('title','Company name')!!}  <br>  
                           

    {!! Form::select('company_id' ,$companydetails,['class'=>'form-control'])!!}

    </div>
</div>
     <div class="form-group">
        <div class="col-sm-8">
     {!! Form::label('title','Events name')!!}                            
    {{Form::text('events_title',$events->events_title,['class'=>'form-control'])}}
    </div>
</div>

<br>
<div class="form-group">
    <div class="col-sm-8">
        {{Form::label('title','Description')}}
    {{Form::textarea('description',$events->description,['class'=>'form-control'])}}
</div>  
</div>


<div class="form-group">
    <div class="col-sm-8">
        {{Form::label('title','Image')}}
    {{Form::file('image',['class'=>'form-control'])}}
</div>
</div>

<div class="form-group">
    <div class="col-sm-2">
{{ Form::hidden('_method', 'PUT') }}
    {{Form::submit('submit',['class'=>'btn btn-primary'])}}
</div>
</div>
  {!! Form::close() !!}      
                                </div>
                               
                            </div>
                            
                                    
                                    </div><!--- -->
                                </div>
                            </div>
                      
                        
                        </div>
                    </div></div>
                                            </div>
                </div>
                <!-- Content Wrapper END -->
@stop
   @section('page-js')
    