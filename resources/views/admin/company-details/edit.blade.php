@extends('layouts.admin-master')
                             <!-- Content Wrapper START -->
@section('content')
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="card">
                           <div class="col-md-12">
            <div class="panel panel-default">

              <div class="panel-body">
                
                <div class="tab-content br-n pn">
                 
                    <div class="row">
                      <div class="col-md-8"><br>
                                 {!!Form::open(['method' => 'POST','action' =>[ 'CompanysController@update', $companydetails->id]])!!}
     <div class="form-group">
        <div class="col-sm-8">
     {!! Form::label('title','Company Name')!!}                            
    {{Form::text('company_name',$companydetails->company_name,['class'=>'form-control'],$companydetails->company_name)}}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-8">
    {{Form::file('company_photo',['class'=>'form-control'])}}
</div>
</div>
<div class="form-group">
        <div class="col-sm-8">
     {!! Form::label('title','Starts Date')!!}                            
    {{Form::text('start_date',$companydetails->start_date,['class'=>'form-control','id'=>'input_time'])}}
    </div>
</div><br>
<div class="form-group">
        <div class="col-sm-8">
     {!! Form::label('title','End Date')!!}                            
    {{Form::text('end_date',$companydetails->end_date,['class'=>'form-control','id'=>'input_endtime'])}}
    </div>
</div><br>

<div class="form-group">
    <div class="col-sm-2">
        {{ Form::hidden('_method', 'PUT') }}
    {{Form::submit('submit',['class'=>'btn btn-primary'])}}
</div>
</div>
  {!! Form::close() !!}   
                                </div>
                               
                            </div>
                            
                                    
                                    </div><!--- -->
                                </div>
                            </div>
                      
                        
                        </div>
                    </div></div>
                                            </div>
                </div>
                <!-- Content Wrapper END -->
@stop
   @section('page-js')
    