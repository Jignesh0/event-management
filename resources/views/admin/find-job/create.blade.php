@extends('layouts.admin-master')
       <!-- Content Wrapper START -->
@section('content')
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="card">
                           <div class="col-md-12">
            <div class="panel panel-default">

              <div class="panel-body">
                <ul class="nav nav-pills m-b-30 ">
                <li class="active nav-item "> <a href="#navpills-2" class="nav-link" data-toggle="tab" aria-expanded="false">List</a> </li>
                  <li class="nav-item"> <a href="#navpills-1" class="nav-link" data-toggle="tab" aria-expanded="false">Add</a> </li>
                     </ul>
                <div class="tab-content br-n pn">
                  <div id="navpills-1" class="tab-pane">
                    <div class="row">
                      <div class="col-md-8">
                                 {!!Form::open(['action' => 'FindJobController@store','method' => 'POST', 'enctype' => 'multipart/form-data'])!!}
     <div class="form-group">
        <div class="col-sm-8">
     {!! Form::label('title','Title')!!}                            
    {{Form::text('title','',['class'=>'form-control'])}}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-8">
    {{Form::file('image',['class'=>'form-control'])}}
</div>
</div>
<div class="form-group">
    <div class="col-sm-8">
        {{Form::label('title','Description')}}
    {{Form::textarea('description','',['class'=>'form-control'])}}
</div>  
</div>
<div class="form-group">
    <div class="col-sm-2">

    {{Form::submit('submit',['class'=>'btn btn-primary'])}}
</div>
</div>
  {!! Form::close() !!}   
                                </div>
                               
                            </div>
                            
                                    
                                    </div>
                                    <div id="navpills-2" class="tab-pane active">
                  <div class="row">
                    <div class="table-responsive "><br><div class="col-md-8">@include('inc.message')</div><br>
                      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                        <thead>
                          <tr>
                            <th class="center">Sl.</th>
                            <th class="center" >Title</th>
                            <th class="center">photo</th>
                            <th class="center">Description</th>

                            <th class="tdAction" align="center" width="120">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                             @foreach($findjobs as $findjob)
                          <tr class="odd" id="tR1">
                           
                            <td data-title="Serial"  width="30" align="right">{{$findjob->id}}</td>
                           <td data-title="Serial"  >{{$findjob->title}}</td>
                           

                            <td><img src="/find_job/{{$findjob->image}}" width="100" height="60" alt="{{$findjob->text}}"></td>
                             <td data-title="Serial"  >{{$findjob->description}}</td>
                            <td data-title="Action" align="center" width="120"><span id="deleteLoader1">&nbsp;</span>

                                
                             <a onclick="return confirm('Do you want to Edit?')" href="{{route('find-job.edit',$findjob->id)}}" class="btn btn-xs btn-green">
                    <i class="fa fa-pencil"></i>
                    </a>


                          {!!Form::open(['action' => ['FindJobController@destroy', $findjob->id], 'method' => 'POST'])!!}
    {{Form::hidden('_method', 'DELETE')}}
    
                              {{Form::submit('Delete', ['class' => 'fa fa-trash-o'])}}
  {!!Form::close()!!}
   @endforeach
                            </td>
                            
                          </tr>

                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
                                </div>
                            </div>
                      
                        
                        </div>
                    </div></div>
                                            </div>
                </div>
                <!-- Content Wrapper END -->
@stop
                <!-- Footer START -->
                