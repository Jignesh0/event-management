<div class="form-container signup-form">
	<div class="form-container signup-form">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#s-developers" data-toggle="tab">Developers</a>
            </li>
            <li><a href="#s-business" data-toggle="tab">Business</a></li>
            <li><a href="#s-school" data-toggle="tab">School</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade in active" id="s-developers">
                 <form action="{{route('register')}}"
                     class="login-form" method='POST' name="developers">
        			 @csrf
                <fieldset>
                	<div class="form-group"><input type="text" class="form-control" name="name"><label>Name</label>
                	<strong>{{ $errors->first('name') }}</strong>
                	</div>
                    <div class="form-group"><input type="text" class="form-control" name="phone"><label>Phone Number</label>
        			<strong>{{ $errors->first('phone') }}</strong>
                    </div>
                    <div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id</label>
        			<strong>{{ $errors->first('email') }}</strong>
                    </div>
                    <div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label>
        			<strong>{{ $errors->first('password') }}</strong>
                    </div>
                    <input type="hidden" name="role" value="developers">
                </fieldset>
                <div class="form-group login-options clearfix">
                    <a href="#" class="login-btn">Existing User?  <span class="purple">Login</span></a>
                </div>
                <div class="form-group">
                    <input type="submit" class="button" value="Get Started Today">
                </div>
                <p>Or</p>
                <ul class="social-login">
                    <li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
                    <li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
                    <li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
                </ul>
               </form>
            </div>
            <div class="tab-pane fade in" id="s-business">
                <form action="{{route('register')}}"
                     class="login-form" method='POST'>
        			       @csrf
                <fieldset>
                	<div class="form-group"><input type="text" class="form-control"><label>Name</label>
                	<strong>{{ $errors->first('name') }}</strong>
                	</div>
                    <div class="form-group"><input type="text" class="form-control"><label>Phone Number</label>
                    <strong>{{ $errors->first('phone') }}</strong></div>
                    <div class="form-group"><input type="email" class="form-control"><label>Email Id</label>
        			<strong>{{ $errors->first('email') }}</strong>
                    </div>
                    <div class="form-group"><input type="password" class="form-control"><label>Password</label>
        			<strong>{{ $errors->first('password') }}</strong>
                    </div>
                    <input type="hidden" name="role" value="business">
                </fieldset>
                <div class="form-group login-options clearfix">
                    <a href="#" class="login-btn">Existing User?  <span class="purple">Login</span></a>
                </div>
                <div class="form-group">
                    <input type="submit" class="button" value="Get Started Today">
                </div>
                <p>Or</p>
                <ul class="social-login">
                    <li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
                    <li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
                    <li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
                </ul>
            </form>
            </div>
            <div class="tab-pane fade in" id="s-school">
              <form action="{{route('register')}}"
                     class="login-form" method='POST'>
        			 @csrf
                <fieldset>
                	<div class="form-group"><input type="text" class="form-control"><label>Name</label>
                	<strong>{{ $errors->first('name') }}</strong>
                	</div>
                    <div class="form-group"><input type="text" class="form-control"><label>Phone Number
                    </label>
        			<strong>{{ $errors->first('phone') }}</strong>
                	</div>
                    <div class="form-group"><input type="email" class="form-control"><label>Email Id</label>
        			<strong>{{ $errors->first('email') }}</strong>
                    </div>
                    <div class="form-group"><input type="password" class="form-control"><label>Password</label>
        			<strong>{{ $errors->first('password') }}</strong>
                    </div>
                    <input type="hidden" name="role" value="school">
                </fieldset>
                <div class="form-group login-options clearfix">
                    <a href="#" class="login-btn">Existing User?  <span class="purple">Login</span></a>
                </div>
                <div class="form-group">
                    <input type="submit" class="button" value="Get Started Today">
                </div>
                <p>Or</p>
                <ul class="social-login">
                    <li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
                    <li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
                    <li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
                </ul>
                </form>
            </div>
        </div>
</div>
</div>