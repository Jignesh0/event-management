<div class="tab-content">
	<div class="tab-pane fade in active" id="developers">
		<form action="{{ route('login') }}" class="login-form" method="POST">
			@csrf
		<fieldset>
			<div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id/ Phone Number</label>
			<strong>{{ $errors->first('email') }}</strong>
			</div>
			<div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label><div class="validate"><img src="img/wrong.svg" alt=""></div>
			<strong>{{ $errors->first('password') }}</strong>
			</div>
		</fieldset>
		<div class="form-group login-options">
			<a href="#">Recover Password</a>
			<span style="display: none;">OTP sent to Mobile</span><a href="#" style="display: none;">Resend</a>
			<a href="#" class="signup-btn">Not Registered? <span class="purple">Signup</span></a>
		</div>
		<div class="form-group">
			<input type="submit" class="button" value="Get Started Today">
		</div>
		<p>Or</p>
		<ul class="social-login">
			<li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
			<li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
			<li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
		</ul>
	</form>
	</div>
	<div class="tab-pane fade in" id="business">
		<form action="{{ route('login') }}" class="login-form" method="POST">
			@csrf
		<fieldset>
			<div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id/ Phone Number</label>
			<strong>{{ $errors->first('email') }}</strong>
			</div>
			<div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label><div class="validate"><img src="img/wrong.svg" alt=""></div>
			<strong>{{ $errors->first('password') }}</strong>
			</div>
		</fieldset>
		<div class="form-group login-options">
			<a href="#">Recover Password</a>
			<a href="#" class="signup-btn">Not Registered? <span class="purple">Signup</span></a>
		</div>
		<div class="form-group">
			<input type="submit" class="button" value="Get Started Today">
		</div>
		<p>Or</p>
		<ul class="social-login">
			<li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
			<li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
			<li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
		</ul>
	</form>
	</div>
	<div class="tab-pane fade in" id="school">
		<form action="{{ route('login') }}" class="login-form" method="POST">
			@csrf
		<fieldset>
			<div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id/ Phone Number</label>
			<strong>{{ $errors->first('email') }}</strong>
			</div>
			<div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label><div class="validate"><img src="img/wrong.svg" alt=""></div>
			<strong>{{ $errors->first('password') }}</strong>
			</div>
		</fieldset>
		<div class="form-group login-options">
			<a href="#">Recover Password</a>
			<a href="#" class="signup-btn">Not Registered? <span class="purple">Signup</span></a>
		</div>
		<div class="form-group">
			<input type="submit" class="button" value="Get Started Today">
		</div>
		<p>Or</p>
		<ul class="social-login">
			<li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
			<li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
			<li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
		</ul>
	</form>
	</div>
</div>
