@extends('layouts.client-master')
@section('content')

	<!-- Login & Signup -->
<div class="login-wrapper">
	<a href="#" class="close-btn"><img src="img/close.svg" alt=""></a>
	<div class="left-section"><h2>Login</h2><h4>Join Over 3 Million Programmers</h4><img src="img/login-bg.png" alt=""></div>
			<div class="login_signup right-section">
				  <div class="form-container">
					 <ul class="nav nav-tabs">
						<li class="active"><a href="#developers" data-toggle="tab">Developers</a></li>
						<li><a href="#business" data-toggle="tab">Business</a></li>
						<li><a href="#school" data-toggle="tab">School</a></li>
					 </ul>
					 <form action="{{ route('login') }}" class="login-form" method="POST">
					 	@csrf
						<div class="tab-content">
							<div class="tab-pane fade in active" id="developers">
								<div class="wrapper otp-send">
								<fieldset>
									<div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id/ Phone Number</label>
									<strong>{{ $errors->first('email') }}</strong>
									</div>
									<div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label><div class="validate"><img src="img/wrong.svg" alt=""></div>
									<strong>{{ $errors->first('password') }}</strong>
									</div>
								</fieldset>
								<div class="form-group login-options">
									<a href="#">Recover Password</a>
									<span style="display: none;">OTP sent to Mobile</span><a href="#" style="display: none;">Resend</a>
									<a href="#" class="signup-btn">Not Registered? <span class="purple">Signup</span></a>
								</div>
								<div class="form-group">
									<input type="submit" class="button" value="Get Started Today">
								</div>
								<p>Or</p>
								<ul class="social-login">
									<li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
									<li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
									<li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
								</ul>
								</div>
							</div>
							<div class="tab-pane fade in" id="business">
								<div class="wrapper">
								<fieldset>
									<div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id/ Phone Number</label>
									<strong>{{ $errors->first('email') }}</strong>
									</div>
									<div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label><div class="validate"><img src="img/wrong.svg" alt=""></div>
									<strong>{{ $errors->first('password') }}</strong>
									</div>
								</fieldset>
								<div class="form-group login-options">
									<a href="#">Recover Password</a>
									<a href="#" class="signup-btn">Not Registered? <span class="purple">Signup</span></a>
								</div>
								<div class="form-group">
									<input type="submit" class="button" value="Get Started Today">
								</div>
								<p>Or</p>
								<ul class="social-login">
									<li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
									<li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
									<li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
								</ul>
								</div>
							</div>
							<div class="tab-pane fade in" id="school">
								<div class="wrapper">
								<fieldset>
									<div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id/ Phone Number</label>
									<strong>{{ $errors->first('email') }}</strong>
									</div>
									<div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label><div class="validate"><img src="img/wrong.svg" alt=""></div>
									<strong>{{ $errors->first('password') }}</strong>
									</div>
								</fieldset>
								<div class="form-group login-options">
									<a href="#">Recover Password</a>
									<a href="#" class="signup-btn">Not Registered? <span class="purple">Signup</span></a>
								</div>
								<div class="form-group">
									<input type="submit" class="button" value="Get Started Today">
								</div>
								<p>Or</p>
								<ul class="social-login">
									<li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
									<li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
									<li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
								</ul>
								</div>
							</div>
						</div>
					  </form>

				</div>
				  <div class="form-container signup-form">
					 	<div class="form-container signup-form">
		                     <ul class="nav nav-tabs">
		                        <li class="active"><a href="#s-developers" data-toggle="tab">Developers</a></li>
		                        <li><a href="#s-business" data-toggle="tab">Business</a></li>
		                        <li><a href="#s-school" data-toggle="tab">School</a></li>
		                     </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="s-developers">
                                <div class="wrapper otp-send">
                                 <form action="{{route('register')}}"
                                     class="login-form" method='POST'>
                     			       @csrf
                                <fieldset>
                                	<div class="form-group"><input type="text" class="form-control" name="name"><label>Name</label>
                                	<strong>{{ $errors->first('name') }}</strong>
                                	</div>
                                    <div class="form-group"><input type="text" class="form-control" name="phone"><label>Phone Number</label>
									<strong>{{ $errors->first('phone') }}</strong>
                                    </div>
                                    <div class="form-group"><input type="email" class="form-control" name="email"><label>Email Id</label>
									<strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    <div class="form-group"><input type="password" class="form-control" name="password"><label>Password</label>
									<strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                    <input type="hidden" name="role" value="developers">
                                </fieldset>
                                <div class="form-group login-options clearfix">
                                    <a href="#" class="login-btn">Existing User?  <span class="purple">Login</span></a>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="button" value="Get Started Today">
                                </div>
                            </form>
                                <p>Or</p>
                                <ul class="social-login">
                                    <li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
                                    <li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
                                    <li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
                                </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="s-business">
                                <div class="wrapper otp-send">
                                <form action="{{route('register')}}"
                                     class="login-form" method='POST' name="business">
                     			       @csrf
                                <fieldset>
                                	<div class="form-group"><input type="text" class="form-control"><label>Name</label>
                                	<strong>{{ $errors->first('name') }}</strong>
                                	</div>
                                    <div class="form-group"><input type="text" class="form-control"><label>Phone Number</label>
                                    <strong>{{ $errors->first('phone') }}</strong></div>
                                    <div class="form-group"><input type="email" class="form-control"><label>Email Id</label>
									<strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    <div class="form-group"><input type="password" class="form-control"><label>Password</label>
									<strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                    <input type="hidden" name="role" value="business">
                                </fieldset>
                                <div class="form-group login-options clearfix">
                                    <a href="#" class="login-btn">Existing User?  <span class="purple">Login</span></a>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="button" value="Get Started Today">
                                </div>
                            </form>
                                <p>Or</p>
                                <ul class="social-login">
                                    <li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
                                    <li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
                                    <li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
                                </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="s-school">
                                <div class="wrapper otp-send">
                              <form action="{{route('register')}}"
                                     class="login-form" method='POST'>
                     			       @csrf
                                <fieldset>
                                	<div class="form-group"><input type="text" class="form-control"><label>Name</label>
                                	<strong>{{ $errors->first('name') }}</strong>
                                	</div>
                                    <div class="form-group"><input type="text" class="form-control"><label>Phone Number
                                    </label>
									<strong>{{ $errors->first('phone') }}</strong>
                                	</div>
                                    <div class="form-group"><input type="email" class="form-control"><label>Email Id</label>
									<strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                    <div class="form-group"><input type="password" class="form-control"><label>Password</label>
									<strong>{{ $errors->first('password') }}</strong>
                                    </div>
                                    <input type="hidden" name="role" value="school">
                                </fieldset>
                                <div class="form-group login-options clearfix">
                                    <a href="#" class="login-btn">Existing User?  <span class="purple">Login</span></a>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="button" value="Get Started Today">
                                </div>
                            </form>
                                <p>Or</p>
                                <ul class="social-login">
                                    <li><a href="#"><img src="img/facebook.svg" alt="">Facebook</a></li>
                                    <li><a href="#"><img src="img/google-plus.svg" alt="">Google</a></li>
                                    <li><a href="#" class="github"><img src="img/github.svg" alt="">Github</a></li>
                                </ul>
                                </div>
                            </div>
                        </div>
                </div>

				</div>
	 		</div>
    </div>

	<div class="swiper-container swiper-container-home">
		<div class="swiper-wrapper">
			<div class="swiper-slide"><a href="#"><div class="slide-title"><h2>Discover Innovation. Create Radical.</h2><h1>be the creator.</h1></div><img src="img/slide/one.png" alt=""></a></div>
			<div class="swiper-slide"><a href="#"><div class="slide-title"><h2>Discover Innovation. Create Radical.</h2><h1>be the creator.</h1></div><img src="img/slide/two.jpg" alt=""></a></div>
			<div class="swiper-slide"><a href="#"><div class="slide-title"><h2>Discover Innovation. Create Radical.</h2><h1>be the creator.</h1></div><img src="img/slide/three.jpg" alt=""></a></div>
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-prev swiper-button-black"></div>
		<div class="swiper-button-next swiper-button-black"></div>
  </div>
@if(Auth::user())
	{{ Auth::user()->name }}
@endif
@stop
